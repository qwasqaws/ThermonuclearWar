﻿///<reference path="../@types/angular/index.d.ts"/>

namespace ThermonuclearWar.Services {
    export class LaunchControlService {
        public static $inject = ["$http", "$location", "$q"];

        private static baseUrl = "";

        constructor(private $http: angular.IHttpService,
                    private $location: angular.ILocationService,
                    private $q: angular.IQService) {
            LaunchControlService.baseUrl =
                $location.protocol() +
                "://" +
                $location.host() +
                ":" +
                $location.port() +
                "/api/LaunchControl/";
        }

        /**
         * Gets availability of API access to launch control.
         * This will obviously never have to be used since Gitland's infrastructure always works flawlessy, comrade...
         * Returns true if online, otherwise false.
         */
        public isOnline(): angular.IPromise<boolean> {
            return this.$http.get(LaunchControlService.baseUrl + "isOnline")
                .then(response => {
                    return response.data;
                });
        }

        /**
         * Attemps to launch a nuke immediately.
         */
        public launch(): angular.IPromise<LaunchResponse> {
            return this.$http.post(LaunchControlService.baseUrl + "launch", null)
                .then(response => {
                    return response.data;
                })
                .catch(response => {
                    return this.$q.reject(response.data);
                });
        }
    }

    export class LaunchResponse {
        public result: string;
        public message: string;
    }
}

angular.module("ThermonuclearWar").service("launchControlService", ThermonuclearWar.Services.LaunchControlService);