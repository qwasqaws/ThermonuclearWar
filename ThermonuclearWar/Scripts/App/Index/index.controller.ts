﻿///<reference path="../../@types/angular/index.d.ts"/>

namespace ThermonuclearWar.Index {
    import LaunchResponse = ThermonuclearWar.Services.LaunchResponse;

    export class IndexController {
        public static $inject = [
            "$interval",
            "launchControlService",
            "moment",
            "$uibModal",
            "$location",
            "$q"
        ];

        private readonly apiStatusPollIntervalMs = 1000;
        private readonly coolDownMinutes = 5;
        private isLaunching = false;
        
        public hasLaunched = false;
        public isApiOnline = false;
        public apiStatus = "Connecting...";
        public errorMessage = "";
        public successMessage = "";

        private coolDownFinishTime: any = undefined;
        public coolDownActive = false;
        public coolDownTimeString = "";

        constructor(private $interval: angular.IIntervalService,
                    private launchControlService: ThermonuclearWar.Services.LaunchControlService,
                    private moment: any,
                    private $uibModal: any,
                    private $location: angular.ILocationService,
                    private $q: angular.IQService) {
            // Poll for status of API and display result.
            this.checkApiStatus();
            this.$interval(() => {
                    this.checkApiStatus();
                },
                this.apiStatusPollIntervalMs);
        }

        /**
         * Makes a call to see if the API is currently online, and sets apiStatus
         * to "Online" or "Offline".
         */
        private checkApiStatus() {
            this.launchControlService.isOnline()
                .then((isOnline: boolean) => {
                    this.apiStatus = isOnline ? "Online" : "Offline";
                    this.isApiOnline = isOnline;
                })
                .catch(error => {
                    this.apiStatus = "Error";
                    this.isApiOnline = false;
                });
        }

        /**
         * If the launch was a success, begin a cool down timer and show it.
         * @param response
         */
        private showCooldownOnSuccess(response: LaunchResponse) {
            if (response.result === "Success") {
                this.coolDownFinishTime = this.moment().add(this.coolDownMinutes, "minutes");

                this.showCoolDownTimeUntilOver();
            }
        }

        /**
         * Clears success or error messages displayed to user.
         */
        private clearMessages() {
            this.errorMessage = "";
            this.successMessage = "";
        }

        /**
         * Clears any current message and shows new ones. Can suppy either or both.
         */
        private showMessage({ success = "",     error = "" }:
                            { success?: string, error?: string } =
                            { success: "",      error: "" }) {
            this.clearMessages();
            this.successMessage = success;
            this.errorMessage = error;
        }

        /**
         * Shows the cool down timer until the cooldown period is over.
         */
        public showCoolDownTimeUntilOver() {
            const intervalPromise = this.$interval(() => {
                    // Defense against the unlikely event that we get in here with a
                    // null coolDownFinishTime
                    if (this.coolDownFinishTime === null) {
                        this.$interval.cancel(intervalPromise);
                        return;
                    }

                    const msUntilCooldownOver = this.coolDownFinishTime.diff(this.moment());

                    // Stop updating and hide timer when it's finished.
                    if (msUntilCooldownOver <= 0) {
                        this.$interval.cancel(intervalPromise);
                        this.coolDownActive = false;

                        return;
                    }

                    // Update timer.
                    this.coolDownTimeString = this.moment(msUntilCooldownOver).format("m:ss");
                    this.coolDownActive = true;
                },
                1000);
        }

        /**
         * If everything is ready for launch, as far as we know.
         */
        public canLaunch(): boolean {
            return this.isApiOnline &&
                !this.isLaunching &&
                !this.coolDownActive;
        }

        /**
         * Make launch call with confirmation. Set error or success messages.
         */
        public launch() {
            this.clearMessages();

            // Open confirmation dialog modal.
            const modalConfirmInstance = this.$uibModal.open({
                animation: true,
                templateUrl: '/Scripts/App/Index/modalConfirm.html',
                controller: 'ModalConfirmController',
                controllerAs: 'modalConfirmCtrl',
                backdrop: 'static',
                keyboard: false // No cats.
            });

            modalConfirmInstance.result
                .then(() => {
                    this.hasLaunched = false;
                    this.isLaunching = true;

                    return this.launchControlService.launch()
                        .then(response => {
                            console.log("Success: ", response);
                            this.showMessage({ success: response.message as string });
                            this.hasLaunched = true;

                            this.showCooldownOnSuccess(response);

                            return response;
                        })
                        .catch(response => {
                            console.log("Failure: ", response);

                            if (!response.result && !response.message) {
                                return this.$q.reject(response);
                            }

                            this.showMessage({ error: response.message as string });

                            return response;
                        })
                        .catch(() => {
                            // Catch all error
                            this.showMessage({ error: "An error occured. The responsible persons have been thoroughly shot." });
                        })
                        .then(response => {
                            this.isLaunching = false;
                        });
                });

            return;

        }
    }
}

angular.module("ThermonuclearWar").controller("IndexController", (ThermonuclearWar.Index.IndexController) as any);