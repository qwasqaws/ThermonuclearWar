﻿///<reference path="../../@types/angular/index.d.ts"/>

namespace ThermonuclearWar.Index {
    export class ModalConfirmController {
        public static $inject = [
            "$uibModalInstance"
        ];

        constructor(private $uibModalInstance: any) {

        }

        public confirm() {
            this.$uibModalInstance.close();
        };

        public cancel() {
            this.$uibModalInstance.dismiss();
        };
    }
}

angular.module("ThermonuclearWar").controller("ModalConfirmController", ThermonuclearWar.Index.ModalConfirmController);