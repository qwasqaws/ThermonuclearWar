﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using ThermonuclearWar.Models;
using ThermonuclearWarApiWrapper;
using ThermonuclearWarApi = ThermonuclearWarApiWrapper.ThermonuclearWar;

namespace ThermonuclearWar.Services
{
    public class LaunchControlService
    {
        private IThermonuclearWar Api { get; }
        private string Launchcode { get; } = "NICEGAMEOFCHESS";
        private int LaunchCoolDownMinutes { get; } = 5;

        private ThermonuclearWarDbContext DbContext { get; set; }

        public LaunchControlService()
        {
            this.DbContext = new ThermonuclearWarDbContext();
            this.Api = new ThermonuclearWarApi();
        }

        /// <summary>
        /// For mocking.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="api"></param>
        public LaunchControlService(ThermonuclearWarDbContext dbContext, IThermonuclearWar api)
        {
            this.DbContext = dbContext;
            this.Api = api;
        }

        /// <summary>
        /// Returns a 423-Locked with an appropriate LaunchResponse for failing the cooldown check.
        /// </summary>
        /// <returns></returns>
        public ResponseMessageResult GetFailedCooldownResponse()
        {
            var lockedResponse = new LaunchResponse()
            {
                Result = "Failure",
                Message = "Less than 5 minutes have passed since last launch"
            };

            var response = new ResponseMessageResult(new HttpResponseMessage((HttpStatusCode) 423)
            {
                Content = new ObjectContent(typeof(LaunchResponse), lockedResponse, new JsonMediaTypeFormatter())
            });
            return response;
        }

        /// <summary>
        /// Works out if the cooldown period between launches has passed.
        /// </summary>
        /// <returns>True if good to go, otherwise false.</returns>
        public bool IsLaunchCooldownPassed()
        {
            // To be on the safe side don't allow any launches within 5 minutes.
            // Sticking to the spec would mean different codes could launch nukes within 
            // 5 mins of each other when passing midnight.

            var recentLaunch = this.DbContext.Launches
                .OrderByDescending(launch => launch.DateTime)
                .FirstOrDefault();
            
            var minCoolDownTime = DateTime.Now.AddMinutes(-this.LaunchCoolDownMinutes);

            var launchCooldownPassed = recentLaunch == null 
                || recentLaunch.DateTime < minCoolDownTime;
            return launchCooldownPassed;
        }

        /// <summary>
        /// Attemps a launch and keeps a record of it if it succeeded.
        /// </summary>
        /// <returns>Reponse from server, or locally generated error.</returns>
        public async Task<LaunchResponse> LaunchAsync()
        {
            var launchCode = DateTime.Now.ToString("yyMMdd") + "-" + this.Launchcode;

            var launchResponse = await Api.Launch(launchCode);

            if (launchResponse.Result == "Success")
            {
                this.DbContext.Launches.Add(new Launch(DateTime.Now, launchCode));
                this.DbContext.SaveChanges();
            }

            return launchResponse;
        }

        /// <summary>
        /// Builds a ResponseMessageResult based on the contents of the <paramref name="launchResponse"/>.
        /// </summary>
        /// <param name="launchResponse"></param>
        /// <returns></returns>
        public ResponseMessageResult GetAppropriateErrorResponseMessage(LaunchResponse launchResponse)
        {
            var httpCode = launchResponse.Message == "Invalid Launch Code"
                ? HttpStatusCode.Unauthorized
                : HttpStatusCode.InternalServerError;

            return this.BuildResponseMessage(httpCode, launchResponse);
        }

        public ResponseMessageResult BuildResponseMessage(HttpStatusCode httpStatusCode, LaunchResponse launchResponse)
        {
            var responseMessageResult = new ResponseMessageResult(new HttpResponseMessage(httpStatusCode)
            {
                Content = new ObjectContent(typeof(LaunchResponse), launchResponse, new JsonMediaTypeFormatter())
            });

            return responseMessageResult;
        }
    }
}