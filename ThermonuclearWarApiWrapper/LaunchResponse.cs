using Newtonsoft.Json;

namespace ThermonuclearWarApiWrapper
{
    public class LaunchResponse
    {
        [JsonProperty("result")]
        public string Result;

        [JsonProperty("message")]
        public string Message;
    }
}